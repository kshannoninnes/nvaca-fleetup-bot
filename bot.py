import logging
import sys

from navbot import get_dotlan_map, get_jump_dist, get_xkcd_url
from slackclient import SlackClient
from slack import Slackbot
from config import SLACK_BOT_TOKEN, BLACKLIST

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create a file handler
# handler = logging.FileHandler('debug.log')
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)

slack = SlackClient(SLACK_BOT_TOKEN)
bot = Slackbot(slack, logger)


@bot.command('ping', help='Check boss presence')
def ping(channel, arg):
    if arg and not arg.startswith(BLACKLIST):
        message = arg
    elif arg.startswith(BLACKLIST):
        message = 'Mentions are not a valid parameter.'
    else:
        message = 'Pong'

    return bot.post_message(channel, message)


@bot.command('help', help='Shows list of supported commands.')
def help(channel, arg):
    if arg is None:
        message = 'Supported commands: *{}*.\n*help _command_* for detailed help.'.format(
            ', '.join(bot._commands.keys()))
    elif arg in bot._command_help:
        message = bot._command_help[arg]
    else:
        message = 'No help entry found for {}'.format(arg)

    return bot.post_message(channel, message)


@bot.command('distance', help='Show distance between 2 systems. Usage: *range _system_ _system_*')
def distance(channel, arg):
    bot.set_typing(channel)
    if arg:
        args = arg.split(' ', 1)
        if len(args) == 2 and not any(element.startswith(BLACKLIST) for element in args):
            dist = get_jump_dist(args)
            if dist and dist > 0:
                message = args[0].upper() + ' to ' + args[1].upper() + ': ' + '{:,.2f}ly'.format(dist)
            else:
                message = 'Invalid arguments. Usage: *distance _system_ _system_*'
        elif not len(args) == 2:
            message = 'Invalid number of systems. Usage: *distance _system_ _system_*'
        else:
            message = 'Mentions are not a valid parameter.'
    else:
        message = 'Invalid number of systems. Usage: *distance _system_ _system_*'

    return bot.post_message(channel, message)


@bot.command('rangemap', help='Get a dotlan map showing all locations in jump range from a system. '
             'Usage: *map _system_ _jdclevel_ _ship_*')
def rangemap(channel, arg):
    bot.set_typing(channel)
    if arg:
        args = arg.split(' ', -1)
        if len(args) == 3 and not any(element.startswith(BLACKLIST) for element in args):
            message = get_dotlan_map(args)
        elif not len(args) == 3:
            message = 'Invalid number of arguments. Usage: *map _system_ _jdc level_ _ship name_*'
        else:
            message = 'Mentions are not a valid parameter.'
    else:
        message = 'Invalid number of arguments. Usage: *map _system_ _jdc level_ _ship name_*'

    return bot.post_message(channel, message)


@bot.command('xkcd', help='Display an XKCD webcomic Usage: *xkcd _number OR random_*')
def xkcd(channel, arg):
    bot.set_typing(channel)
    if arg:
        args = arg.split(' ', -1)
        if len(args) == 1 and not any(element.startswith(BLACKLIST) for element in args):
            message = get_xkcd_url(args[0])
        elif not len(args) == 1:
            message = 'Invalid number of arguments. Usage: *xkcd _number OR random_*'
        else:
            message = 'Mentions are not a valid parameter.'
    else:
        message = 'Invalid number of arguments. Usage: *xkcd _number OR random_*'

    return bot.post_message(channel, message)


if __name__ == '__main__':
    bot.run()
